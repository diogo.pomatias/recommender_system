FROM python:3

COPY . /src/.

WORKDIR /src

RUN pip install -r /src/requirements.txt

