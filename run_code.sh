#!/bin/bash

MOVIE_NAME=$1
N_SIMILAR=$2

./create_image.sh

docker run --rm -it -v $PWD:/src/ -t recommender bash -c "python generates_recommendations.py"
#docker run --rm -v $PWD:/src/ -m=4g --memory-swap 9g --oom-kill-disable --kernel-memory 3g  -it recommender /bin/bash
