## Recommender System

The goal of this project is to create a progressive more complex recommender system in two challenges.

- In the first challenge,the input are a movie and N similar movies. Hence, the recommender only takes into consideration similarity between movies' own characteristics. Namely, similarity between movies was computed by considering the following characteristics:  movie collection(if any), genre, keywords, top 3 most important role actors and director. Then, amongst the most similar movies (higher cosine similarity), a weighted calculation between the latter similarity value and the imdb rating average was computed to calculate the final recommendation. To avoid the fact that a certain movie may have a higher/lower rating but was only rated by a few number of people, the recommender only considers movies with more than a certain threshold of vote ratings. The model outputs N similar movies to the one selected.

- In the second challenge, the inputs are a movie, N similar movies, and the user id. Hence, the recommender makes personalized decisions for each user similar to the selected movie. To do so, a prediction algorithm (KNNWithMeans) was used to predict the rating that a user would give to a movie that he has not rated before. However, the algorithm only takes into account similarity between movies ratings, and disregards the movies' features. To tackle this issue, a hybrid approach was implemented where the final recommendation is a weighted calculation between the movies content similarity (from challenge #1) and the highest predicted ranked movies for a particular user. In the case where there are no user prediction for a particular movie (because no user in the data has rated those movies), only the content similarity value was taken into account. 

#Note: 
Please bear in mind that if you run the project in a Mac, you need to increase the docker virtual size: it is set to a maximum of 2gb. Ideally, you would change it to the maximum allowed 8gb, in the docker GUI settings option.

To create the docker image after cloning the repo, first run:
```
./create_image.sh
```

To run the tests:
```
./pytest.sh
```

To run the code that produces the recommendations, just run:
```
./run_code.sh
```
