import pytest
from generates_recommendations import *
import pandas as pd
from pandas._testing import assert_frame_equal, assert_series_equal
import os
import numpy as np
pd.options.mode.chained_assignment = None  # default='warn'

@pytest.fixture
def datasets(tmpdir):
    dataset_1 = str(os.path.join(tmpdir, 'test_name1.csv'))
    dataset_2 = str(os.path.join(tmpdir, 'test_name2.csv'))
    
    datasets = [
	dataset_1,
        dataset_2
    ]

    return datasets

def test_reads_datasets(datasets):
    pd.DataFrame({'a': [1, 2]}).to_csv(datasets[0])
    pd.DataFrame({'b': [3, 4]}).to_csv(datasets[1])

    expected = {
        'name1': pd.read_csv(datasets[0]),
        'name2': pd.read_csv(datasets[1]),
    }

    result = reads_datasets(datasets)

    assert_frame_equal(expected['name1'], result['name1'])
    assert_frame_equal(expected['name2'], result['name2'])

def test_clean_jsoninfo():
    test_values = [
        np.nan,
        {'id': 1},
        {'id': 2, 'name': 'Romantic Thriller'},
        [{'id': 2, 'name': 'Action'},  {'id': 3, 'name': 'Comedy Fiction'}],
        [{'name': 'TARANTINO', 'job': 'Director'}, {'name': 'BrAD PIT', 'job': 'Actor'}],
        {'name': 'TaRantino', 'job': 'director'}
    ]
    
    expected = {
        'test1_2': '',
        'test3': 'romanticthriller',
        'test4': 'action comedyfiction',
        'test5': 'tarantinodirector bradpitactor',
	'test6': 'tarantinodirector'
    }
    
    assert expected['test1_2'] == clean_jsoninfo(test_values[0])
    assert expected['test1_2'] == clean_jsoninfo(test_values[1])
    assert expected['test3'] == clean_jsoninfo(test_values[2])
    assert expected['test4'] == clean_jsoninfo(test_values[3])
    assert expected['test5'] == clean_jsoninfo(test_values[4])
    assert expected['test6'] == clean_jsoninfo(test_values[5])
 
def test_get_director():
    test_values = [
       [ {'name': 'BrAD PIT', 'job': 'Actor'}, {'name': 'TARANTINO', 'job': 'Director'}],
       [ {'name': 'Quentin TaRantino', 'job': 'director'}]
    ]
    
    expected = {
        'test1': 'tarantino',
        'test2': 'quentintarantino'
    }

    assert expected['test1'] == get_director(test_values[0])
    assert expected['test2'] == get_director(test_values[1])

def test_get_top3actors():
    test_values = [
       [ 
         {'name': 'BrAD PIT', 'order': 0}, 
         {'name': 'TARANTINO', 'order': 1}, 
         {'name': 'Matthew McConaughey', 'order': 2}, 
         {'name': 'Russel Crowe', 'order': 3}
       ],
       [ {'name': 'Quentin TaRantino', 'order': 0}]
    ]
    
    expected = {
        'test1': 'bradpit tarantino matthewmcconaughey',
        'test2': 'quentintarantino'
    }

    assert expected['test1'] == get_top_3actors(test_values[0])
    assert expected['test2'] == get_top_3actors(test_values[1])

def test_normalize_weighted_rating():
    test_df = pd.DataFrame({'weighted_rating':  [2,10,4,6]})
                              
    expected_df =  pd.DataFrame({'weighted_rating':   [0,1,0.25,0.5]})

    assert_frame_equal(expected_df, normalize_weighted_rating(test_df))

def test_get_imdb_weighted_rating():
    test_df = pd.DataFrame({'vote_count': [0, 0, 0, 18, 25, 54, 65, 557],
                           'vote_average': [0, 0, 0, 5.4, 5.6, 7, 5.6, 8.9]})
    
    expected = {
        'test1':  pd.DataFrame({'vote_count': [18, 25, 54, 65, 557],
                                'vote_average': [5.4, 5.6, 7, 5.6, 8.9],
                                'weighted_rating': [5.793, 5.857, 6.922, 5.720, 8.858]}),
        'test2':  pd.DataFrame({'vote_count': [25, 54, 65, 557],
                                'vote_average': [5.6, 7, 5.6, 8.9],
                                'weighted_rating': [6.000, 6.865, 5.812, 8.817]})
    }
    
    resulting_df = {
        'result1': get_imdb_weighted_rating(test_df,10),
        'result2': get_imdb_weighted_rating(test_df,20)
    }

    assert_frame_equal(expected['test1'],
        resulting_df['result1'].apply(lambda x: np.round(x,3))[['vote_count','vote_average','weighted_rating']].reset_index(drop=True))
    assert_frame_equal(expected['test2'],
        resulting_df['result2'].apply(lambda x: np.round(x,3))[['vote_count','vote_average','weighted_rating']].reset_index(drop=True))

def test_aggregate_allcolumns():
    test_df = pd.DataFrame({'title': ['toystory', 'hamlet'], 
                          'genres': ['action comedy', ''],
                          'Director': ['tarantino', 'mathieu'],
                          'cast': ['bradpit russelcroew antunes', 'mathieu']
                          })    
    
    expected = {
        'test1': 'toystory action comedy tarantino bradpit russelcroew antunes',
        'test2': 'hamlet  mathieu mathieu'
    }

    assert expected['test1'] == aggregate_allcolumns(test_df,['title','genres','Director','cast'])[0]
    assert expected['test2'] == aggregate_allcolumns(test_df,['title','genres','Director','cast'])[1]
    
def test_replicate_data_wtimes():
    test_values = {
        'test1': 'action action',
        'test2': 'comedy action comedy',
        'test3': 'thriller  comedy animation'
    }

    expected = {
        'test1': 'action action action action action action ',
        'test2': 'comedy action comedy ',
        'test3': 'thriller  comedy animation thriller  comedy animation '
    }

    assert expected['test1'] == replicate_data_wtimes(test_values['test1'],3)
    assert expected['test2'] == replicate_data_wtimes(test_values['test2'],1)
    assert expected['test3'] == replicate_data_wtimes(test_values['test3'],2)

def test_apply_weights_to_features():
    test_df = {
        'test1': pd.DataFrame({'title': ['toystory', 'hamlet'], 
                               'genres': ['action comedy', ''],
                               'Director': ['tarantino', 'fernandes'],
                               'cast': ['bradpit russelcroew antunes', 'mathieu']
                          }),
        'test2': pd.DataFrame({'title': ['toystory', 'hamlet'], 
                               'genres': ['action comedy', ''],
                               'Director': ['tarantino', 'fernandes'],
                               'cast': ['bradpit russelcroew antunes', 'mathieu']
                          })
    }
     
    weighted_columns = {
        'test1': {
            'title': 2,
            'Director': 3
        },
        'test2': {
            'title': 2,
            'genres': 2,
            'cast': 1
        }   
    }    
 
    expected = {
        'test1': pd.DataFrame({'title': ['toystory toystory ', 'hamlet hamlet '], 
                               'genres': ['action comedy', ''],
                               'Director': ['tarantino tarantino tarantino ', 'fernandes fernandes fernandes '],
                               'cast': ['bradpit russelcroew antunes', 'mathieu']
                          }),
        'test2': pd.DataFrame({'title': ['toystory toystory ', 'hamlet hamlet '], 
                               'genres': ['action comedy action comedy ', '  '],
                               'Director': ['tarantino', 'fernandes'],
                               'cast': ['bradpit russelcroew antunes ', 'mathieu ']
                          })
    }

    assert_frame_equal(expected['test1'], apply_weights_to_features(test_df['test1'], weighted_columns['test1'] ))
    assert_frame_equal(expected['test2'], apply_weights_to_features(test_df['test2'], weighted_columns['test2']))

def test_get_highest_weighted_similarity_wr():
    test_df = pd.DataFrame({'original_title': ['toystory', 'hamlet', 'toystory2', 'inception', 'cars'], 
                            'title': ['Toy Story', 'Hamlet', 'Toy Story 2', 'Inception', 'Cars'],
                            'normalized_wr': [0.6, 0.7, 0.8, 0.3, 0.2]
                            })
    list_of_movies = [
        ('toystory', 0.3),
        ('hamlet', 0.56),
        ('inception', 0.3),
        ('toystory2',0.58),
        ('cars',0.52)
    ]

    expected = {
        'test1': ['Toy Story 2'],
        'test2': ['Toy Story 2', 'Hamlet', 'Toy Story']
    }
    
    assert expected['test1'] == get_highest_weighted_similarity_wr(1, list_of_movies, test_df)
    assert expected['test2'] == get_highest_weighted_similarity_wr(3, list_of_movies, test_df)
 
    
