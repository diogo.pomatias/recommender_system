import pandas as pd 
from ast import literal_eval
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import sys
import numpy as np
import warnings
from surprise import Dataset, Reader
from surprise.model_selection import cross_validate
from surprise import KNNWithMeans
warnings.filterwarnings("ignore")
pd.options.mode.chained_assignment = None  # default='warn'

DATASETS = [
    'datasets/movies_metadata.csv',
    'datasets/movies_keywords.csv',
    'datasets/movies_credits.csv',
    'datasets/movies_ids.csv',
    'datasets/user_ratings.csv'
] 

JSON_COLUMNS = [
    'belongs_to_collection',
    'genres',
    'production_companies',
    'production_countries',
    'spoken_languages', 
    'keywords',
    'crew',
    'cast'
]

NUMERIC_FEATURES = [
    'popularity',
    'vote_count',
    'vote_average',
    'movieId'
]

WEIGHTED_COLUMNS = {
    'belongs_to_collection': 3,
    'Director': 2
}

RECOMMENDER_FEATURES = [
    'belongs_to_collection',
    'genres',
    'cast',
    'Director',
    'keywords'
]

FEATURES_TO_INCLUDE = [
    'original_title',
    'title',
    'belongs_to_collection',
    'genres',
    'keywords',
    'crew',
    'cast',
    'vote_count',
    'vote_average',
    'movieId',
    'id'    
]

def reads_datasets(datasets = DATASETS):
    datasets_df_dict = {}
    for dataset in datasets:
        datasets_df_dict[dataset.split('_')[-1].split('.')[0]] = pd.read_csv(dataset)	
    return datasets_df_dict

def parse_strinfied_to_object(df):
    for feature in JSON_COLUMNS:
        if feature in df.columns:
            df[feature] = df[feature].fillna('[]').apply(literal_eval)
    for feature in NUMERIC_FEATURES:
        if feature in df.columns:
            df[feature] = df[feature].fillna(0)
    df = df.fillna('')
    return df

def clean_jsoninfo(x):
    outcome = []
    if isinstance(x,list):
        for element, key in enumerate(x):
            if 'job' in x[element].keys() and 'name' in x[element].keys():
                outcome.append(str.lower(str(key['name']+key['job']).replace(" ", "")))
            elif 'name' in x[element].keys():
                outcome.append(str.lower(key['name'].replace(" ", "")))
        return ' '.join(outcome)
    elif isinstance(x,dict):
        if 'job' in x.keys() and 'name' in x.keys():
            return str.lower(str(x['name']+x['job']).replace(" ",""))
        elif 'name' in x.keys():
            return str.lower(x['name'].replace(" ", ""))    
    return ''

def get_director(x):    
    for i in x: 
        if str.lower(i['job']) == 'director':
            return str.lower(i['name'].replace(" ", ""))
    return ''

def get_top_3actors(x):
    top3actors = []
    for i, key in enumerate(x):
        if 'order' in x[i].keys():
            if key['order'] <3:   
                top3actors.append(str.lower(key['name'].replace(" ", "")))
    return ' '.join(top3actors)

def normalize_weighted_rating(x):
    return (x - np.min(x)) / (np.max(x) - np.min(x))

def get_imdb_weighted_rating(movies_df, threshold=20):
    #movies with zero vote count don't matter for the vote average
    #Recommend movies that have at least "threshold" votes.
    movies_mean = movies_df[movies_df['vote_count']>0]['vote_average'].mean()
    movies_df = movies_df[movies_df['vote_count']>=threshold]
    v = movies_df['vote_count']
    R = movies_df['vote_average']
    movies_df['weighted_rating'] = ((v)/(v+threshold))*R + ((threshold)/(v+threshold))*movies_mean
    movies_df['normalized_wr'] = normalize_weighted_rating(movies_df['weighted_rating'])
    return movies_df

def parses_json(movies_df):
    movies_df = parse_strinfied_to_object(movies_df)
    movies_df['cast'] = movies_df['cast'].apply(get_top_3actors)
    movies_df['Director'] = movies_df['crew'].apply(get_director)
    for feature in JSON_COLUMNS[:-1]:
        if feature in movies_df.columns:
            movies_df[feature] = movies_df[feature].apply(clean_jsoninfo)
    return movies_df

def merge_datasets(datasets_df_dict):
    #CGiven that the id of each movie is the same on the first tree datasets, lets merge them together
    datasets_df_dict['metadata']['id'] = datasets_df_dict['metadata']['id'].apply(lambda x: pd.to_numeric(x, errors = 'coerce'))
    movies_df = datasets_df_dict['metadata'].merge(datasets_df_dict['keywords'], how = 'left')
    movies_df = movies_df.merge(datasets_df_dict['credits'], how = 'left')

    datasets_df_dict['ids']['tmdbId'] = datasets_df_dict['ids']['tmdbId'].apply(lambda x: pd.to_numeric(x, errors = 'coerce'))
    datasets_df_dict['ids'].rename(columns = {'tmdbId':'id'}, inplace= True)
    movies_df = movies_df.merge(datasets_df_dict['ids'], how = 'left')
    #same movies appear more than once in the dataset. Decided to keep the movie with the most vote count because more people interacted/commented
    # on it.
    movies_df = movies_df.sort_values('vote_count', ascending = False).drop_duplicates('original_title').sort_index()
    movies_df.reset_index(drop=True,inplace=True)
    return movies_df

def aggregate_allcolumns(movies_df, columns_to_compute):
    return  movies_df[columns_to_compute].apply(lambda x:
        ' '.join([x[feature] for feature in columns_to_compute ]), axis=1)

def replicate_data_wtimes(x, weight):
    output = ''
    for i in range(weight):
        output += x + " "
    return output      
 
def apply_weights_to_features(movie_df, weighted_columns=WEIGHTED_COLUMNS):
    for feature, weight in weighted_columns.items():
         movie_df[feature] = movie_df[feature].apply(lambda x: replicate_data_wtimes(x,weight))
    return movie_df

def get_highest_weighted_similarity_wr(n, list_of_movies, movies_df):
    top_similar_movies_df = pd.DataFrame(list_of_movies, columns = ['original_title','similarity'])
    top_similar_movies_df = top_similar_movies_df.merge(movies_df[['title','original_title', 'normalized_wr','movieId']], how = 'inner')
    top_similar_movies_df['weighted_similarity_wr'] = 0.5 * top_similar_movies_df['similarity'] + 0.5 * top_similar_movies_df['normalized_wr']
    return list(top_similar_movies_df.nlargest(n,'weighted_similarity_wr')['title']), top_similar_movies_df

def get_n_similar_movies(n, movie_name, movies_df, columns_to_compute):  
    movies_df = get_imdb_weighted_rating(movies_df, movies_df['vote_count'].quantile(0.6))
    #apply different weights to certain features based on importance
    movies_df = apply_weights_to_features(movies_df) 

    concatenated_series = aggregate_allcolumns(movies_df,columns_to_compute)
    count_vect = CountVectorizer(stop_words='english')
    count_matrix = count_vect.fit_transform(concatenated_series)
    cos_matrix = cosine_similarity(count_matrix, count_matrix) 
    cos_df = pd.DataFrame(cos_matrix, columns = movies_df['original_title'], index = movies_df['original_title'])
    #return 40 + user's similar movies number wanted and return the ones with best weighted rating.  
    #It aims to minimize the possibility that a "similar" movie with low weighted rating is recommended.    
    list_of_movies = [(name, cos_df.loc[name,movie_name]) for name in cos_df[movie_name].nlargest(n+41).index.tolist() if name != movie_name]
    top_similar_movies, top_similar_movies_df = get_highest_weighted_similarity_wr(n, list_of_movies, movies_df)
    return top_similar_movies, top_similar_movies_df
    
def get_recommendation(movie_name, n_similar = 5):
    dataset_df_dict=  reads_datasets()
    movies_df = merge_datasets(dataset_df_dict)
    movies_df =  movies_df[FEATURES_TO_INCLUDE]
    movies_df['original_title'] = movies_df['original_title'].apply(lambda x: str.lower(x.replace(" ", "")))
    if movies_df[movies_df['original_title'] ==  movie_name].empty:
        print("Movie not found in the database! Please choose other movie.")
        return []
    else:
        movies_df = parses_json(movies_df)
        list_of_movies, top_similar_movies_df = get_n_similar_movies(n_similar, movie_name,  movies_df, RECOMMENDER_FEATURES)
        return list_of_movies, dataset_df_dict['ratings'], top_similar_movies_df 

def fit_train_predict(user_df):
    # Algorithm calculates the cosine similarity between items and predicts the user rating of an unseen movie based on the ratings he has given to those similar items. Maximum number of similar movies to be chosen to predict the rating for unseen movies is 40.
    reader = Reader(rating_scale = (0,5))
    user_data = Dataset.load_from_df(user_df[['userId', 'movieId', 'rating']], reader)
    trainset = user_data.build_full_trainset()
    sim_options = {'name': 'cosine', 'min_support': 3, 'user_based': False}
    algo = KNNWithMeans(k = 40, min_k = 3, sim_options = sim_options, verbose = False)
    results = cross_validate(algo, data = user_data, measures = ['RMSE'], cv = 3)
    #print(results['test_rmse'].mean())
    algo.fit(trainset)
    test_set = trainset.build_anti_testset()
    predictions = algo.test(test_set)
    return predictions

def get_user_top_n_recommendations(predictions, userid, user_df):
    top_n = {userid: []}
    for uid, iid, true_r, est, _ in predictions:
        if uid == userid:
            top_n[uid].append((iid, est))
     
    for user_ratings in top_n.values():
        user_ratings.sort(key = lambda x: x[1], reverse = True)
        top_n[userid] = user_ratings
    
    top_n_df = pd.DataFrame(top_n[userid], columns = ['movieId', 'rating'])
    top_n_df['userId'] = userid
    user_df_real_ratings = user_df[user_df['userId'] == userid][['movieId','rating','userId']]
    top_n_df = pd.concat([top_n_df, user_df_real_ratings])
    return top_n_df

def get_user_specific_recommendations(content_based_df, n, userid, user_df ):
    predictions = fit_train_predict(user_df)
    user_based_df = get_user_top_n_recommendations(predictions, userid, user_df)
    user_based_df['rating'] = normalize_weighted_rating(user_based_df['rating'])
    user_similarity_df = content_based_df.merge(user_based_df[['movieId','rating']], how = 'left')
    user_similarity_df['weighted_content_user_rating'] = 0.4 * user_similarity_df['weighted_similarity_wr'] + 0.6 * user_similarity_df['rating']
    # when we don't have any user feedback on the movie - cold start - let's assume the value of item similarity
    user_similarity_df.loc[user_similarity_df['weighted_content_user_rating'].isnull(), 'weighted_content_user_rating'] = user_similarity_df['weighted_similarity_wr']
    return list(user_similarity_df.nlargest(n,'weighted_content_user_rating')['title'])

if __name__ == '__main__':
    challenge = int(input("Please enter the challenge (1,2): ")) 
    movie_name = input("Please enter a movie: ")
    movie_name_lower = str.lower(movie_name.replace(" ","")) 
    n_similar = int(input("Please enter the number of similar movies: "))
    if challenge == 1:
        print("Searching for similar movies ... \n")
        content_recommendations, _, _ = get_recommendation(movie_name_lower, n_similar)
        print("Movies recommended based on {} are: \n {}".format(movie_name, content_recommendations))
    elif challenge == 2:
        userid = int(input("Please select a userId between 1 and 671: "))
        print("Searching for similar movies ... \n") 
        _ , user_df, content_based_df = get_recommendation(movie_name_lower, n_similar)
        content_user_recommendations = get_user_specific_recommendations(content_based_df, n_similar, userid, user_df)
        print("Movies recommended for user {} based on {} and his/her profile are: \n {}".format(userid, movie_name, content_user_recommendations))
